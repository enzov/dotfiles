#!/bin/bash

#
# DWM status bar
#
# @see https://github.com/joestandring/dwm-bar
#

SEP_OPEN='['
SEP_CLOSE=']'


dwm_date() {
    printf "%s" "$(date "+%a %d/%m/%Y %T")"
}

dwm_resources () {
	# get all the infos first to avoid high resources usage
	free_output=$(free -h | grep Mem)	
	# Used and total memory
	mused=$(echo $free_output | awk '{print $3}')
	mtot=$(echo $free_output | awk '{print $2}')
	# CPU usage
	cpu=$(top -bn1 | grep Cpu | awk '{print $2}')%
    echo "Mem. $mused/$mtot Cpu. $cpu" > /tmp/_dwm_resources
}

dwm_alsa () {
	local status=$(amixer sget Master | tail -n1 | sed -r "s/.*\[(.*)\]/\1/")
    local vol=$(amixer get Master | tail -n1 | sed -r "s/.*\[(.*)%\].*/\1/")
    printf "Vol. "
    if [ "$status" = "off" ]; then
        printf "Muted"
    else
        printf "%s%%" "$vol"
    fi
}

dwm_voidlinux_updates() {
    num_upd=$(xbps-install -nuM | wc -l)
    echo "$num_upd upd" > /tmp/_dwm_voidlinux_updates
}


parallelize_60() {
    while true
    do
        dwm_voidlinux_updates &
        sleep 60
    done
}

parallelize_10() {
    while true
    do
        dwm_resources &
        sleep 10
    done
}


echo "" > /tmp/_dwm_resources
echo "" > /tmp/_dwm_voidlinux_updates

parallelize_60 &
parallelize_10 &

while true
do
    # Append results of each func one by one to a string
    bar_content=""

    bar_content="$bar_content $SEP_OPEN$(cat /tmp/_dwm_resources)$SEP_CLOSE"    
    bar_content="$bar_content $SEP_OPEN$(cat /tmp/_dwm_voidlinux_updates)$SEP_CLOSE"

    bar_content="$bar_content $SEP_OPEN$(dwm_alsa)$SEP_CLOSE"
    bar_content="$bar_content $(dwm_date) "

    xsetroot -name "$bar_content"
    sleep 1
done

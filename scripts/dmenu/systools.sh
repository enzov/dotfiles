#!/bin/sh

# Petit launcher dmenu permettant de lancer les scripts
# présents dans le repertoire "actions"

ACTIONS_DIR=$(dirname $(readlink -f "$0"))
ACTIONS_DIR="$ACTIONS_DIR/actions"

nb_actions=0
choices=''
for s in $ACTIONS_DIR/*.sh; do
    [ ! -x "$s" ] && continue
    act=$(basename "$s" ".sh")
    [ -z $choices ] && choices="$act" || choices="$choices\n$act"
    nb_actions=$((nb_actions+1))
done

t="Reboot\nHalt"
[ -z $choices ] && choices="$t" || choices="$choices\n$t"
nb_actions=$((nb_actions+2))

if command -v arandr >/dev/null 2>&1; then
    [ -z $choices ] && choices="ARandR" || choices="$choices\nARandR"
    nb_actions=$((nb_actions+1))
fi


# Pour vertical rajouter :  -l $nb_actions
action=$(printf "$choices" | dmenu -b  -fn '-*-fixed-*-*-*-*-10-70-*-*-*-*-*-*' -nb '#222222' -nf '#7D7D7D' -sb '#7D7D7D' -sf '#222222' -i -p 'Actions: ')

case $action in
    Reboot) sudo shutdown -r now;;
    Halt) sudo shutdown -h now;;
    ARandR) exec arandr;;
    *)
        act_file="$ACTIONS_DIR/$action.sh"
        [ ! -x "$s" ] && echo "Action non executable!" || exec $act_file
    ;;
esac